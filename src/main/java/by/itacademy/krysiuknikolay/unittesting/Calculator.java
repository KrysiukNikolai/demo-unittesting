package by.itacademy.krysiuknikolay.unittesting;

public class Calculator {
    //Method Sum
    public int sum(int a, int b) {
        int reuslt = 0;
        if (0 <= a & a < 10 && 0 <= b & b < 10) {
            reuslt = a + b;
        }
        if (10 <= a & a < 20 & 10 <= b & b < 20 && 20 <= (a + b) & (a + b) < 30) {
            reuslt = 20;
        }
        if (10 <= a && a < 20 & 10 <= b & b < 20 && 30 <= (a + b) && (a + b) < 40) {
            reuslt = 30;
        }
        if (a >= 20 && b >= 20) {
            reuslt = 40;
        }
        if (a < 0 || b < 0) {
            reuslt = 0;
        }
        return reuslt;
    }

    // Method Divide
    public double divide(double a, double b) {
        double result = 0;
        if (b <= 0) {
            result = 0;
        }
        if (a >= b) {
            result = a / b;
        }
        if (a < b) {
            result = 1;
        }
        return result;
    }

    // Method Multiplay
    public int multiply(int a, int b) {
        int result = 0;
        if (a < 0 || b < 0) {
            result = -1;
        }
        if (0 <= a & a < 10 && 0 <= b & b < 10) {
            result = a * b;
        }
        if (10 <= a & a < 100 && 10 <= b & b < 100 && 100 <= a * b & a * b < 1000) {
            result = 100;
        }
        if (10 <= a & a < 100 & 10 <= b & b < 100 && 1000 <= a * b & a * b < 10000) {
            result = 1000;
        }
        if (a >= 100 || b >= 100) {
            result = 10000;
        }
        return result;
    }

    // Method subtract
    public int subtract(int a, int b) {
        int result = 0;
        if (a >= b & a >= 0 & b >= 0) {
            result = a - b;
        }
        if (a >= b & a >= 0 & b < 0) {
            result = a;
        }
        if (a < b) {
            result = b;
        }
        return result;
    }
}
