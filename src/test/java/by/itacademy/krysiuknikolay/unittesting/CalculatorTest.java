package by.itacademy.krysiuknikolay.unittesting;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void calcSumPositiveTwenty() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(10, 10);
        Assert.assertEquals(20, actual);
        //Тестируем положительным методом на половине выделенных значений  20.
    }

    @Test
    public void CalcSumTestMinusOne() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(-2, -1);
        Assert.assertEquals(0, actual);
        // Тестируем граничное значение -1
    }

    @Test
    public void calcSumTestZero() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(0, 0);
        Assert.assertEquals(0, actual);
        // Тестируем граничное значение 0
    }

    @Test
    public void calcSumTestOne() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(0, 1);
        Assert.assertEquals(1, actual);
        // Тестируем граничное значение 1
    }

    @Test
    public void CalcSumTestThirtyNine() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(39, 20);
        Assert.assertEquals(40, actual);
        // Тестируем граничное значение 39.
    }

    @Test
    public void calcSumTestForty() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(40, 20);
        Assert.assertEquals(40, actual);
        // Тестируем граничное значение 40
    }

    @Test
    public void calcSumTestFortyOne() {
        Calculator calculator = new Calculator();
        int actual = calculator.sum(41, 21);
        Assert.assertEquals(40, actual);
        // Тестируем граничное значение 41
    }

    // Тестирование метода Divide.-----------------------------------------------------------
    @Test
    public void calcTestDividePositive() {
        Calculator calculator = new Calculator();
        double actual = calculator.divide(6, 2);
        Assert.assertEquals(3, actual, 0.01);
        // Тестируем методом положительного тестирования.
    }

    @Test
    public void calcTestdivideMinusOne() {
        Calculator calculator = new Calculator();
        double actual = calculator.divide(-6, 2);
        Assert.assertEquals(1.0, actual, 0.01);
        // Тестируем  -1
    }

    @Test
    public void calcTestDivide() {
        Calculator calculator = new Calculator();
        double actual = calculator.divide(6, 2);
        Assert.assertEquals(3.0, actual, 0.01);
        // Тестируем 0 Ошибка!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }

    // Тестирование метода Multiply----------------------------------------------------------
    @Test
    public void calcMultPositiveFifty() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(50, 50);
        Assert.assertEquals(1000, actual);
        //Тестируем положительным методом на половине выделенных значений  20.
    }

    @Test
    public void calcMultTestMinusOne() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(-2, -1);
        Assert.assertEquals(-1, actual);
        // Тестируем граничное значение -1
    }

    @Test
    public void calcMultTestZero() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(0, 0);
        Assert.assertEquals(0, actual);
        // Тестируем граничное значение 0
    }

    @Test
    public void calcMultTestOne() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(0, 1);
        Assert.assertEquals(0, actual);
        // Тестируем граничное значение 1
    }

    @Test
    public void sumTestNinetyNine() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(99, 99);
        Assert.assertEquals(1000, actual);
        // Тестируем граничное значение 99.
    }

    @Test
    public void sumTestHundred() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(100, 100);
        Assert.assertEquals(10000, actual);
        // Тестируем граничное значение 100
    }

    @Test
    public void sumTestHundredOne() {
        Calculator calculator = new Calculator();
        int actual = calculator.multiply(101, 101);
        Assert.assertEquals(10000, actual);
        // Тестируем граничное значение 101
    }

    //Тестируем Метод subtract---------------------------------------------------------------------
    @Test
    public void subtractTestPositive() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(8, 4);
        Assert.assertEquals(4, actual);
    // Тестирование на позитивный тест.
    }
    @Test
    public void subtractTestMinusOne() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(-1, 4);
        Assert.assertEquals(4, actual);
        // Тестирование -1.
    }
    @Test
    public void subtractTestZero() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(0, 4);
        Assert.assertEquals(4, actual);
        // Тестирование 0
    }
    @Test
    public void subtractTestOne() {
        Calculator calculator = new Calculator();
        int actual = calculator.subtract(1, 4);
        Assert.assertEquals(4, actual);
        // Тестирование на позитивный тест.
    }

}
